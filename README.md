# Short Link Service.
## How to install?

cp .env.example .env

cd development && docker-compose build && docker-compose up

docker exec -it shortlinkservice bash

composer install

php artisan migrate

