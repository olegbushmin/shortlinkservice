<?php

use App\Http\Controllers\LinkController;
use Illuminate\Support\Facades\Route;

Route::prefix('link')
    ->controller(LinkController::class)
    ->group(function () {
        Route::post('', 'store')->name('create');
    });