<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    public function getShortLinkAttribute(string $shortLink): string
    {
        return config('app.url') . "/" . $shortLink;
    }
}
