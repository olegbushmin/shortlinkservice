<?php

namespace App\Services;

use App\Models\Link;
use Illuminate\Support\Str;

class LinkService
{
    const LINK_LIFETIME_IN_MINUTES = 5;
    const LINK_LENGTH = 10;

    public function store(string $fullLink): Link
    {
        $link = $this->checkIfExistByFullLink($fullLink);

        if ($link) {
            return $link;
        }

        $shortLink = Str::random(self::LINK_LENGTH);

        return $this->saveLink($shortLink, $fullLink);
    }

    public static function checkIfExistByFullLink(string $fullLink): ?Link
    {
        return Link::query()
            ->where('full_link', $fullLink)
            ->where('created_at', '>=', now()->subMinutes(self::LINK_LIFETIME_IN_MINUTES))
            ->first();
    }

    public static function checkIfExistByShortLink(string $shortLink): ?Link
    {
        return Link::query()
            ->where('short_link', $shortLink)
            ->where('created_at', '>=', now()->subMinutes(self::LINK_LIFETIME_IN_MINUTES))
            ->first();
    }

    protected function saveLink(string $shortLink, string $fullLink): Link
    {

        $link = new Link();
        $link->full_link = $this->prepareLink($fullLink);
        $link->short_link = $shortLink;

        try {
            $link->saveOrFail();
        } catch (\Throwable $exception) {
            //TODO: if need a custom handle of exception
            throw $exception;
        }

        return $link;
    }

    protected function prepareLink(string $url): string
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = 'https://' . $url;
        }

        return $url;
    }
}