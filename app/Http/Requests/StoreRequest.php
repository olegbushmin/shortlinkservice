<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'link' => [
                'string',
                'required',
                'url'
            ]
        ];
    }
}
