<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Http\Resources\LinkResource;
use App\Models\Link;
use App\Services\LinkService;
use Illuminate\Http\RedirectResponse;

class LinkController extends Controller
{
    public function __construct(protected readonly LinkService $linkService)
    {
    }

    public function store(StoreRequest $request): LinkResource
    {
        $data = $request->validated();

        /** @var Link $link */
        $link = $this->linkService->store($data['link']);

        return new LinkResource($link);
    }

    public function show(string $shortLink): RedirectResponse
    {
        if (empty($shortLink)) {
            abort(404, 'Link does not exist');
        }

        $link = LinkService::checkIfExistByShortLink($shortLink);

        if (!$link) {
            abort(410, 'The link does not exist');
        }

        return redirect()->away($link->full_link);
    }
}
